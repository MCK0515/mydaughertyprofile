const express = require('express');
const pg = require('pg');
const path = require('path');
const SQL = require('sql-template-strings');

const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const profileRouter = require('./routes/profiles');
const clientRouter = require('./routes/clients')

const app = express();

var allowCrossDomain = function (req, res, next) {
    if ('OPTIONS' == req.method) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
        res.send(200);
    } else {
        next();
    }
};

app.use(allowCrossDomain);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('', indexRouter);
app.use('/api/profiles', profileRouter);
app.use('/api/clients', clientRouter);

module.exports = app;
