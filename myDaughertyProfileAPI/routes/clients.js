const express = require('express');
const router = express.Router();
const pool = require('../database');
const SQL = require('sql-template-strings');
const formidable = require('formidable');
const fs = require('fs')




/* GET users listing. */
router.get('', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    let id = +req.params.id;
    console.log(id);

    pool.query(SQL`SELECT * from client`, (err, data) =>{
        if(err){
            throw err
        }

        res.json(data.rows);

    });

});

router.get('/engagements', async function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    let clients;
    let client_rows;
    let engagement_rows;
    let hash = Object.create(null);

    client_rows =  await new Promise(function (resolve, reject) {
        pool.query(SQL`SELECT * from client ORDER BY client_name ASC`, (err, data) =>{
            if(err){
                throw err
            }

            resolve(data.rows);

        });
    });
    engagement_rows = await new Promise(function (resolve, reject) {
        pool.query(SQL`select * from engagement`, (err, data) =>{
            if(err){
                throw err
            }
            resolve(data.rows)

        });
    });

    Promise.all([client_rows,engagement_rows]).then(function (values){
        client_rows = values[0];

        engagement_rows = values[1];
        clients = client_rows.map((hash => client => hash[client.client_id] = {client_id: client.client_id, client_name: client.client_name, client_location: client.client_location ,engagements: []})(hash));
        engagement_rows.forEach((hash => a => hash[a.client_id].engagements.push({ engagement_id: a.engagement_id, engagement_name:a.engagement_name}))(hash));

        res.send(clients);
    });

});

router.get('/current_consultants/:id', async function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    let consultants;
    let engagement_rows;
    let profiles;
    let filteredProfiles;
    let hash = Object.create(null);
    let id = req.params.id;

    consultants =  await new Promise(function (resolve, reject) {
        pool.query(SQL`SELECT * from consultant ORDER BY first_name ASC`, (err, data) =>{
            if(err){
                throw err
            }

            resolve(data.rows);

        });
    });
    engagement_rows = await new Promise(function (resolve, reject) {
        pool.query(SQL`select con.consultant_id, eng.engagement_id, eng.engagement_name, cl.client_id, cl.client_name, cer.start_date, cer.end_date from con_eng_role cer join consultant con on cer.con_id = con.consultant_id join engagement eng on cer.eng_id = eng.engagement_id join client cl on eng.client_id = cl.client_id order by cer.end_date;`, (err, data) =>{
            if(err){
                throw err
            }
            resolve(data.rows)


        });
    });

    Promise.all([consultants,engagement_rows]).then(function (values){
        consultants = values[0];
        engagement_rows = values[1];
        profiles = consultants.map((hash => consultants => hash[consultants.consultant_id] = {consultant_id: consultants.consultant_id, first_name:consultants.first_name, last_name: consultants.last_name, line_of_service:consultants.line_of_service, email:consultants.email, slack:consultants.slack, bio:consultants.bio ,job_title: consultants.job_title, picture: consultants.picture,engagements: []})(hash));
        engagement_rows.forEach((hash => a => hash[a.consultant_id].engagements.push({ consultant_id:a.consultant_id, engagement_id: a.engagement_id, engagement_name:a.engagement_name, client_id:a.client_id, client_name: a.client_name, start_date: a.start_date, end_date: a.end_date }))(hash));


        filteredProfiles = profiles.filter(profile =>{

            if(profile.engagements.length > 0){
                //console.log(profile.engagements[profile.engagements.length-1])
                return profile.engagements[profile.engagements.length-1].client_id == id;
            }
            // return profile.engagements[profile.engagements.length].client_id === id;
        });
        res.send(filteredProfiles);
    });


});


module.exports = router;
