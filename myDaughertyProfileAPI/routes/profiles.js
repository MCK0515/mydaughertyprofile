const express = require('express');
const router = express.Router();
const pool = require('../database');
const SQL = require('sql-template-strings');
const formidable = require('formidable');
const fs = require('fs')


/* GET users listing. */
router.get('/', async function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  let consultants;
  let engagement_rows;
  let profiles;
  let hash = Object.create(null);

  consultants =  await new Promise(function (resolve, reject) {
    pool.query(SQL`SELECT * from consultant ORDER BY first_name ASC`, (err, data) =>{
          if(err){
              throw err
          }

          resolve(data.rows);

      });
  });
 engagement_rows = await new Promise(function (resolve, reject) {
     pool.query(SQL`select con.consultant_id, eng.engagement_id, eng.engagement_name, cl.client_id, cl.client_name, cer.start_date, cer.end_date from con_eng_role cer join consultant con on cer.con_id = con.consultant_id join engagement eng on cer.eng_id = eng.engagement_id join client cl on eng.client_id = cl.client_id order by cer.end_date;`, (err, data) =>{
         if(err){
             throw err
         }
         resolve(data.rows)


     });
 });

 Promise.all([consultants,engagement_rows]).then(function (values){
     consultants = values[0];
     engagement_rows = values[1];
     profiles = consultants.map((hash => consultants => hash[consultants.consultant_id] = {consultant_id: consultants.consultant_id, first_name:consultants.first_name, last_name: consultants.last_name, line_of_service:consultants.line_of_service, email:consultants.email, slack:consultants.slack, bio:consultants.bio ,job_title: consultants.job_title, picture: consultants.picture,engagements: []})(hash));
     engagement_rows.forEach((hash => a => hash[a.consultant_id].engagements.push({ consultant_id:a.consultant_id, engagement_id: a.engagement_id, engagement_name:a.engagement_name, client_name: a.client_name, start_date: a.start_date, end_date: a.end_date }))(hash));

     res.send(profiles);
 });


    });

/* GET users listing. */
router.get('/:id', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  let id = +req.params.id;
  console.log(id);

  // pool.query(SQL`SELECT * from consultant WHERE consultant_id = ${id}`, (err, data) =>{
  //   if(err){
  //     throw err
  //   }
  //
  //   res.json(data.rows);
  //
  // });
    pool.query(SQL`select con.consultant_id, con.first_name, con.last_name, con.line_of_service, con.slack, con.email, con.bio, con.job_title, con.picture, role_name from consultant con
                   join con_eng_role cer on con.consultant_id = cer.con_id
                   join role r on r.role_id = cer.role_id
                   
                   where con.consultant_id = ${id};`, (err, data) =>{
        if(err){
            throw err
        }

        res.json(data.rows);

    });

});

router.put('/:id', function (req, res, next) {
    res.header("Access-Contro-Allow-Header", "Content-Type, Authorization, Content-Length, X-Requested-With, Access-Control-Allow-Origin");
    res.header("Access-Control-Allow-Origin", "*");

    // res.header("Access-Control-Allow-Methods", "PUT");
    pool.query(SQL `UPDATE consultant SET bio = ${req.body.bio}, slack = ${req.body.slack}, email= ${req.body.email} WHERE consultant_id = ${req.params.id}`, (err, data) => {
        if (err) {
            throw err
        }
    });
    console.log(req.body);
    res.status(200);
    res.send();
});


router.post('/picture', function (req, res, next) {
    res.header("Access-Contro-Allow-Header", "Content-Type, Authorization, Content-Length, X-Requested-With, Access-Control-Allow-Origin");
    res.header("Access-Control-Allow-Origin", "*");

    console.log('picture')
    console.log(req.body);
    let form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
       console.log(files)
        let oldPath = files.myFile.path;
        let newPath = __dirname + "/../public/images/" + files.myFile.name;
        fs.rename(oldPath, newPath, function (err) {
            if(err){
                console.log(err)
                throw err;
            }

        });

        pool.query(SQL `UPDATE consultant set picture = ${files.myFile.name} where consultant_id = ${parseInt(files.myFile.name.charAt(0))}`,(err, data) =>{
        if(err){
            console.log(err)
            throw err
        }
        })

        });


    res.status(200);
    res.send();
});

module.exports = router;
