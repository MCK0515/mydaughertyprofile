import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProfileService } from '../profiles/service/profile.service';
import { IClient } from '../../app/profiles/models/client';
import { Subscription } from 'rxjs';
import _ from 'lodash';
import { MatDialogModule, MatDialog } from '@angular/material';
import { ClientEngagementsComponent } from '../clients/client-engagements.component';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {

  clients: IClient[] = [];
  subscription: Subscription;
  filterBy: string = '';
  filteredClients: IClient[];
  regularDistribution: number = 100 / 4;
  args: string;
  apiURL: string = environment.apiURL;

  // filter through clients
  constructor(private profileService: ProfileService, public dialog: MatDialog) { }

  ngOnInit() {
    this.subscription = this.profileService.getClients().subscribe(
      clients => {
        console.log('HERE' + clients);
        this.clients = clients;
        this.filteredClients = clients;
        console.log(this.clients[0]);
      }
    )
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  openDialog(clientName: string): void {
    console.log(clientName);
    const dialogRef = this.dialog.open(ClientEngagementsComponent, {
      width: '500px',
      data: {name: clientName},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  filter(): void {
    console.log(this.filterBy);
    if (this.filterBy === '') {
      console.log(true);
      this.filteredClients = this.clients;
      return;
    }
    let args = this.filterBy.toLowerCase();
    let returnArray: IClient[] = [];
    let query = args.split(' ');


    let arr = [];

    this.clients.forEach(function (client) {
      arr.push(client.client_name.toLowerCase());
      console.log(query);
      console.log(arr);
      if (_.differenceWith(query, arr).length === 0) {
        returnArray.push(client);
      }
      arr = [];
    });

    this.filteredClients = returnArray;
  }

}
