import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByclientComponent } from './byclient.component';

describe('ByclientComponent', () => {
  let component: ByclientComponent;
  let fixture: ComponentFixture<ByclientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByclientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
