import { Component, OnInit } from '@angular/core';
import { IProfile } from '../profiles/models/profile';
import { Subscription } from 'rxjs';
import { ProfileService } from '../profiles/service/profile.service';
import _ from 'lodash';
import { ActivatedRoute } from '@angular/router';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-byclient',
  templateUrl: './byclient.component.html',
  styleUrls: ['./byclient.component.scss']
})
export class ByClientComponent implements OnInit {

  profiles: IProfile[] = [];
  subscription: Subscription;
  filterBy: string = "";
  regularDistribution: number = 100/ 4;
  args: string = "";
  filters: string[] = ['first_name+last_name'];
  currentEnagement: string;
  apiURL: string = environment.apiURL;

  constructor(private profileService: ProfileService, private route: ActivatedRoute) {}

  ngOnInit() {
    let id = +this.route.snapshot.paramMap.get("id");
    this.subscription = this.profileService.getClientByConsultant(id).subscribe(
      profiles =>{
        this.profiles = profiles;
        console.log(profiles)
        this.currentEnagement = this.profiles[0].engagements[this.profiles[0].engagements.length -1].client_name;

      }

    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
