import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ClientEngagementsComponent } from '../clients/client-engagements.component';

@Component({
  selector: 'app-engagements-popup',
  templateUrl: './engagements-popup.component.html',
  styleUrls: ['./engagements-popup.component.scss']
})
export class EngagementsPopupComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ClientEngagementsComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
