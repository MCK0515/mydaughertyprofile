import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngagementsPopupComponent } from './engagements-popup.component';

describe('EngagementsPopupComponent', () => {
  let component: EngagementsPopupComponent;
  let fixture: ComponentFixture<EngagementsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngagementsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngagementsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
