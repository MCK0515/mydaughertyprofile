import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientEngagementsComponent } from './client-engagements.component';

describe('ClientEngagementsComponent', () => {
  let component: ClientEngagementsComponent;
  let fixture: ComponentFixture<ClientEngagementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientEngagementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientEngagementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
