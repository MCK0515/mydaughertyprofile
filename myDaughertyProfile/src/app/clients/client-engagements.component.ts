import {Component, OnDestroy, OnInit, Inject} from '@angular/core';
import {Subscription} from 'rxjs';
import {ProfileService} from '../profiles/service/profile.service';
import {IClient} from '../profiles/models/client';
import {ActivatedRoute} from '@angular/router';
import { MAT_DIALOG_DATA } from '@angular/material';



@Component({
  selector: 'app-client-engagements',
  templateUrl: './client-engagements.component.html',
  styleUrls: ['./client-engagements.component.scss']
})
export class ClientEngagementsComponent implements OnInit, OnDestroy {

  engagementSub: Subscription;
  anchorSub;
  clients: IClient[];
  anchor = '';


  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private service: ProfileService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.engagementSub = this.service.getClientEngagements().subscribe(
      clients =>{
        this.clients = clients;
    });

    this.anchorSub = this.route.fragment.subscribe((fragment: string) => {
      this.anchor = fragment;
    })
  }

  ngOnDestroy(): void {
    this.engagementSub.unsubscribe();
  }

}
