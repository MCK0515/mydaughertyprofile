import {Component} from "@angular/core";
import {User} from "./models/user";
import {MatSnackBar} from "@angular/material";
import {Credentials} from './models/credentials';
import {Router} from '@angular/router';
import {LoginService} from '../profiles/service/login.service';
import {Subscription} from 'rxjs';

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent {
  public loggedInUser: User;
  public credentials: Credentials = new Credentials();
  private loginSubscription: Subscription;

  constructor(private readonly matSnackBar: MatSnackBar, private router: Router, private loginService: LoginService) {

  }


  public login(): void {
    this.loginSubscription = this.loginService.login(this.credentials).subscribe(()=>{

      this.router.navigate(['profiles'])}, (error: string) => {
      this.matSnackBar.open("Failed to login user!", undefined, {
        duration: 3000,
        verticalPosition: "top"
      });
    });
  }
}
