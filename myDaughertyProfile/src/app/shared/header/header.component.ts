import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../login/models/user';
import {Observable, Subscription} from 'rxjs';
import {LoginService} from '../../profiles/service/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{ //implements OnInit, OnDestroy
  public user$: Observable<User>;

  private userSubscription: Subscription;

  constructor(private loginService: LoginService, private router: Router) {
  }

  logout(){
   //this.user$ = undefined;
    this.loginService.logout();
    this.router.navigate([''])
  }

  ngOnInit(): void {
    this.user$ = this.loginService.getUser();
    console.log(this.user$);
  }
  //
  // ngOnDestroy(): void {
  //   this.userSubscription.unsubscribe();
  //
  // }
}

