import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {IProfile} from '../models/profile';
import {catchError} from 'rxjs/operators';
import {IEngagement} from '../models/engagement';
import {IClient} from '../models/client';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  //private apiURL = 'http://localhost:7000';
  private apiURL = environment.apiURL;

  constructor(private http: HttpClient) { }

  public getProfiles(): Observable<IProfile[]>{
    return this.http.get<IProfile[]>(`${this.apiURL}/api/profiles`).pipe(

      catchError(this.handleError)
    );
  }

  public getProfilesById(id: number): Observable<IProfile[]>{
    return this.http.get<IProfile[]>(`${this.apiURL}/api/profiles/${id}`).pipe(

      catchError(this.handleError)
    );
  }

  public updateUser(profile: IProfile): Observable<IProfile> {
    let header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',

    });
    console.log('sending')
    return this.http.put<IProfile>(`${this.apiURL}/api/profiles/${profile.consultant_id}`, profile).pipe(

      catchError(this.handleError)
    );
  }

  upload(file: File, id: string): Observable<Object> {
    console.log('submitting picture');
    let extension: string = '';


      const uploadData = new FormData();

      console.log('UploadData: '+uploadData);

      uploadData.append('myFile', file, id+file.name.slice(-4));
      return this.http.post(`${this.apiURL}/api/profiles/picture`, uploadData).pipe(
        catchError(this.handleError)
      );

    }

    getClients(): Observable<IClient[]>{
      return this.http.get<IClient[]>(`${this.apiURL}/api/clients`).pipe(
        catchError(this.handleError)
      );
    }

    getClientEngagements(): Observable<IClient[]>{
      return this.http.get<IClient[]>(`${this.apiURL}/api/clients/engagements`).pipe(
        catchError(this.handleError)
      );
    }

    getClientByConsultant(id: number): Observable<IProfile[]>{
      return this.http.get<IProfile[]>(`${this.apiURL}/api/clients/current_consultants/${id}`).pipe(
        catchError(this.handleError)
      );
    }


  private handleError(err: HttpErrorResponse) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
