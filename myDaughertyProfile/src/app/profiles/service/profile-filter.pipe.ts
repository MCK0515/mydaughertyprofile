import {Pipe, PipeTransform} from '@angular/core';
import {IProfile} from '../models/profile';
import  _  from 'lodash';

@Pipe({
  name: 'profileFilter'
})
export class ProfileFilterPipe implements PipeTransform {

  transform(value: IProfile[], args?: string): IProfile[] {

    if (args == '') {
      return value;
    }
    args = args.toLowerCase().trim();
    let returnArray: IProfile[] = [];
    let query = args.split(' ');


    let arr = [];

    console.log(args);
    value.forEach(function(profile) {
      if ((profile.line_of_service.toLowerCase()+ profile.first_name.toLowerCase()+profile.last_name.toLowerCase()).includes(args)) {
        returnArray.push(profile);
      } else if ((profile.first_name.toLowerCase()+' '+profile.last_name.toLowerCase()).includes(args)) {
        returnArray.push(profile);
      // } else if (profile.last_name.toLowerCase().includes(args)) {
      //   returnArray.push(profile);
      } else if (profile.engagements.some(e => e.client_name.toLowerCase().includes(args))) {
        returnArray.push(profile);
      } else if (profile.engagements.some(e => e.engagement_name.toLowerCase().includes(args))) {
        returnArray.push(profile);
      }
    });

    if(value.some(e => e.first_name.toLowerCase().includes(args))){

    }

      return returnArray;

    //return false;
  }

}
