import { Injectable } from '@angular/core';
import {Observable, ReplaySubject, Subscriber} from 'rxjs';
import {User} from '../../login/models/user';
import {Credentials} from '../../login/models/credentials';
import {LocalStorageService} from './local-storage.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  private userSubject = new ReplaySubject<User>();

  constructor(private localStorageService: LocalStorageService, private http: HttpClient) {

    this.init();
  }

  private init(): void {
    const userJsonStr: string | null = this.localStorageService.getItem("user");

    if (userJsonStr !== null) {
      const user: User = JSON.parse(userJsonStr);
      this.userSubject.next(user);
    }
  }

  public login(credentials: Credentials): Observable<void> {
    return Observable.create((observer: Subscriber<void>) => {
      if (credentials.username && credentials.password) {
        credentials.password = undefined;
        const user = new User();
        user.username = credentials.username;
        this.userSubject.next(user);
        this.localStorageService.setItem("user", JSON.stringify(user));
        observer.next();
      } else {
        observer.error("Failed to login user!");
      }
    });
  }

  public logout(){
    this.localStorageService.removeItem("user");
    this.userSubject.next(undefined);
  }

  public getUser(): Observable<User> {
    return this.userSubject.asObservable();
  }
}
