import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ProfileService } from '../service/profile.service';
import { IProfile } from '../models/profile';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs';
import {NgForm} from '@angular/forms';



@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit, OnDestroy{
  profile: IProfile;
  subscription: Subscription;
  editUserSubscription: Subscription;
  pictureSubscription: Subscription;
  test: string;
  fileSelected: boolean = false;
  selectedFile: File;
  pageSubmit: boolean = false;

  @ViewChild('file')
  fileElement: ElementRef;

  constructor(private profileService: ProfileService, private route: ActivatedRoute, private router: Router) {

  }

  ngOnInit() {
    this.test = 'Name';
    let id = +this.route.snapshot.paramMap.get('id');

    this.subscription = this.profileService.getProfilesById(id).subscribe(
      profile => {
        this.profile = profile[0];
        console.log(this.profile);
      }
    )
  }

  updateProfile(form: NgForm): void {
    this.pageSubmit = true;
    console.log(this.profile);
    if (form.valid) {
      this.editUserSubscription = this.profileService.updateUser(this.profile).subscribe()

      if(this.fileSelected){
        this.pictureSubscription = this.profileService.upload(this.selectedFile, ''+this.profile.consultant_id).subscribe();
      }
      setTimeout(()=> this.router.navigate(['profiles',this.profile.consultant_id]), 2000);

    }
  }

  navigateToProfile(): void
  {
    this.router.navigate(['profiles',this.profile.consultant_id])
  }
  onFileChanged(event) {
    console.log('file Selected');
    let ext = this.fileElement.nativeElement.value.substr(-3);
    this.selectedFile = event.target.files[0];
    if(this.goodFileSize(this.selectedFile.size) && this.goodFileType(ext)){
      this.fileSelected = true;
    }
    else{
      this.fileElement.nativeElement.value = "";
    }

  }

  goodFileSize(size: number): boolean{
    if(size / 1024 / 1024 < 4){
      return true
    }
    alert('File Size is too large, file will not be uploaded');
    return false;
  }

  goodFileType(ext: string): boolean{
    if(ext === 'jpg' || ext === 'png'){
    return true;
    }
    alert(`File type ${ext} is not supported, acceptable file types are .jpg or .png`);
    return false;
  }


  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
