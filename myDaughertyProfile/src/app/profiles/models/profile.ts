import {IEngagement} from './engagement';

export interface IProfile {
  consultant_id: number;
  first_name: string;
  last_name: string;
  line_of_service: string;
  slack: string;
  email: string;
  bio: string;
  job_title: string;
  picture: string;
  role_name: string;
  engagements: IEngagement[];

}
