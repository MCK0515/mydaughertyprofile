import {IEngagement} from './engagement';

export interface IClient {
  client_id: number;
  client_name: string;
  client_location: string;
  logo: string;
  engagements: IEngagement[];
}
