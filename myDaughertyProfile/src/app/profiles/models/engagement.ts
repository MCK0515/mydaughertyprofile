export interface IEngagement {
  engagement_id: number;
  engagement_name: string;
  client_name: string;
  client_id: number;
  start_date: Date;
  end_date: Date;

}
