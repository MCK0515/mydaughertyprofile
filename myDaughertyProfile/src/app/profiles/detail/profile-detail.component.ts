import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { IProfile } from "../models/profile";
import { ProfileService } from "../service/profile.service";
import { IEngagement } from "../models/engagement";
import {environment} from '../../../environments/environment';

// Declare var tableau because it is referencing the tableau js library
declare var tableau: any;

@Component({
  selector: "app-profile-detail",
  templateUrl: "./profile-detail.component.html",
  styleUrls: ["./profile-detail.component.scss"]
})
export class ProfileDetailComponent implements OnInit, OnDestroy {
  profile: IProfile;
  subscription: Subscription;
  viz: any;
  apiURL: string = environment.apiURL;

  constructor(
    private route: ActivatedRoute,
    private profileService: ProfileService
  ) {}

  ngOnInit() {
    let id = +this.route.snapshot.paramMap.get("id");

    this.subscription = this.profileService
      .getProfilesById(id)
      .subscribe(profiles => {
        console.log("DETAIL" + profiles);
        this.profile = profiles[0];
      });
    var placeholderDiv = document.getElementById("vizContainer");
    // Replace this url with the url of your Tableau dashboard
    var url =
      "https://public.tableau.com/views/timelineGantt2/ConTL?:embed=y&:display_count=yes";
    var options = {
      hideTabs: true,
      width: "100%",
      height: "370px",

      "Consultant Id": id,

      onFirstInteractive: function() {
        // The viz is now ready and can be safely used.
        console.log("Run this code when the viz has finished     loading.");
      }
    };
    // Creating a viz object and embed it in the container div.
    this.viz = new tableau.Viz(placeholderDiv, url, options);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
