import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProfileService} from '../service/profile.service';
import {IProfile} from '../models/profile';
import {Subscription} from 'rxjs';
import { environment } from '../../../environments/environment';
import _ from 'lodash';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent implements OnInit, OnDestroy{

  profiles: IProfile[] = [];
  subscription: Subscription;
  filterBy: string = "";
  filteredProfiles: IProfile[];
  regularDistribution: number = 100/ 4;
  args: string = "";
  apiURL: string = environment.apiURL;

  filters: string[] = ['engagements.consultant_id.client_name','first_name+last_name+line_of_service', 'line_of_service+last_name+first_name', 'line_of_service+first_name+last_name', 'last_name+line_of_service'];
  constructor(private profileService: ProfileService) { }

  ngOnInit() {
    this.subscription = this.profileService.getProfiles().subscribe(
      profiles =>{
        console.log('HERE'+ profiles);
        this.profiles = profiles;
        this.filteredProfiles = profiles;
        console.log(this.profiles[0])
      }
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  filter(): void{
    console.log(this.filterBy);
    if (this.filterBy == '') {
      console.log(true);
      this.filteredProfiles = this.profiles;
      return;
    }
    let args = this.filterBy.toLowerCase();
    let returnArray: IProfile[] = [];
    let query = args.split(' ');


    let arr = [];

    this.profiles.forEach(function(profile) {
      arr.push(profile.first_name.toLowerCase(), profile.last_name.toLowerCase(), profile.line_of_service.toLowerCase());
      profile.engagements.forEach(function(engagement) {
        arr.push(engagement.client_name.toLowerCase());
      });
      console.log(query);
      console.log(arr);
      if(_.differenceWith(query, arr).length === 0){
        returnArray.push(profile);
      }
      arr = [];
    });

    this.filteredProfiles= returnArray;
  }

}
