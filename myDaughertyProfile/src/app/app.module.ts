import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import { ProfileListComponent } from './profiles/list/profile-list.component';
import { ProfileDetailComponent } from './profiles/detail/profile-detail.component';
import {ProfileService} from './profiles/service/profile.service';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {CommonModule} from '@angular/common';
import {
  MatButtonModule,
  MatCardModule, MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatDialogModule
} from '@angular/material';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgPipesModule} from 'ng-pipes';
import {ProfileFilterPipe} from './profiles/service/profile-filter.pipe';
import {ProfileEditComponent} from './profiles/edit/profile-edit.component';
import { ClientListComponent } from './client-list/client-list.component';
import {ClientEngagementsComponent} from './clients/client-engagements.component';
import { ByClientComponent } from './byclient/byclient.component';
import { EngagementsPopupComponent } from './engagements-popup/engagements-popup.component';
import {LocalStorageService} from './profiles/service/local-storage.service';

@NgModule({
  declarations: [
    AppComponent,
    ProfileListComponent,
    ProfileDetailComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    ProfileFilterPipe,
    ProfileEditComponent,
    ClientListComponent,
    ClientEngagementsComponent,
    ByClientComponent,
    EngagementsPopupComponent

  ],
  imports: [
    HttpClientModule,
    NgPipesModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    MatDividerModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatMenuModule,
    MatButtonModule,
    MatSnackBarModule,
    MatCardModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    NgPipesModule,
    MatDialogModule,
    RouterModule.forRoot([
      {path: 'profiles', component: ProfileListComponent},
      {path: 'profiles/:id', component: ProfileDetailComponent},
      {path: 'login', component: LoginComponent},
      {path: 'editProfile/:id', component: ProfileEditComponent},
      {path: 'clients', component: ClientListComponent},
      {path: 'engagements', component: ClientEngagementsComponent},
      {path: 'byclient/:id', component: ByClientComponent},
      {path: 'buttontest', component: EngagementsPopupComponent},
      {path: '', redirectTo: '/login', pathMatch: 'full'}
    ])
  ],
  providers: [LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
